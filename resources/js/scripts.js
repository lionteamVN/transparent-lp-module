$(document).ready(function(){
    //2 sliders lớn
    $('.owl-carousel').owlCarousel({
		loop:true,
	    margin:10,
	    nav:true,
	    responsive:{
	        1000:{
	            items:1
	        }
    	}
    });
    $('.owl-carousel-sm').owlCarousel({
		loop:true,
	    margin:10,
	    nav:true,
	    responsive:{
	        1000:{
	            items:1
	        }
    	}
    });
    
});

//Create effect when click next and prev button

$(document).ready(function(){
  $(".owl-next").click(function(){
 
  	//Thanh Rbar bên trái.
    $(".Rpagdot").animate({
    	top: "100px"
    });
    $(".dotshow").animate({
    	opacity: '0'
    });
    $(".dotbg").animate({
    	opacity: '0'
    });
    $(".dothide").animate({
    	opacity: '1'
    });
    $(".dothide").css({
    	"transition": "all .1s ease-in"
    });
    $(".dotbg2").animate({
    	opacity: '1'
    });

    //Phần text bên Rleft-content
    $(".text-down").animate({
    	opacity: '1',
    	fontSize: '600%',
    	top: '150px'
    });
    $(".text-show").animate({
    	opacity: '.2',
    	fontSize: '350%',
    	top: '300px'
    });
    $(".text-up").animate({
    	opacity: '0'
    });
    $(".text-down-hide").animate({
    	opacity: '.2',
    	top: '-50px'
    });
    $(".Rbtn-explore").css({
    	"background-color": "rgba(170, 80, 50, 0.9)",
    	"transition": "all .3s ease-in .2s",
        'transform': 'rotateX(360deg)'
    });

    //Change bacground-img
    $(".lg-slider-1").css({
        'backface-visibility': 'hidden',
        'transform': 'rotateX(90deg)',
        'transition': 'transform 1s ease-in-out .2s',
        'background-color': 'transparent',
        'perspective': '1000px',
        'transform-style': 'preserve-3d'
    });
    $(".lg-slider-2").css({
        'backface-visibility': 'hidden',
        'transform': 'rotateX(360deg)',
        'transition': 'transform 1s ease-in-out .2s',
        'background-color': 'transparent',
        'perspective': '1000px',
        'transform-style': 'preserve-3d'
    });

    //Change bg-color next & prev button
    $(".Rnxt-btn").css({
        'background-color': 'rgba(180,150,120,0.6)'
    });
    $(".Rprev-btn").css({
        'background-color': 'rgba(180,150,120,0.6)'
    });

    //Create effect of the img(next)
    // Image 1
    $(".lg-slider-1 .slider-st").css({
        'backface-visibility': 'hidden',
        'transform': 'rotateY(90deg)',
        'transition': 'transform .5s',
        'background-color': 'transparent',
        'perspective': '1000px',
        'transform-style': 'preserve-3d'       
    });
     $(".lg-slider-2 .slider-st").css({
        'backface-visibility': 'hidden',
        'transform': 'rotateY(360deg)',
        'transition': 'transform .5s',
        'background-color': 'transparent',
        'perspective': '1000px',
        'transform-style': 'preserve-3d'       
    });
    // Image 2
    $(".lg-slider-1 .slider-nd").css({
        'backface-visibility': 'hidden',
        'transform': 'rotateY(90deg)',
        'transition': 'transform .7s',
        'background-color': 'transparent',
        'perspective': '1000px',
        'transform-style': 'preserve-3d'   
    });
    $(".lg-slider-2 .slider-nd").css({
        'backface-visibility': 'hidden',
        'transform': 'rotateY(360deg)',
        'transition': 'transform .7s',
        'background-color': 'transparent',
        'perspective': '1000px',
        'transform-style': 'preserve-3d'   
    });
    // Image 3
    $(".lg-slider-1 .slider-rd").css({
        'backface-visibility': 'hidden',
        'transform': 'rotateY(90deg)',
        'transition': 'transform .9s',
        'background-color': 'transparent',
        'perspective': '1000px',
        'transform-style': 'preserve-3d'
    });
    $(".lg-slider-2 .slider-rd").css({
        'backface-visibility': 'hidden',
        'transform': 'rotateY(360deg)',
        'transition': 'transform .9s',
        'background-color': 'transparent',
        'perspective': '1000px',
        'transform-style': 'preserve-3d'
    });
  });
//--------------------------------------------------------------------

  $(".owl-prev").click(function(){
    
  	//Thanh Rbar bên trái.
    $(".Rpagdot").animate({
    	top: "-10px"
    });
   	$(".dotshow").animate({
    	opacity: '1'
    });
    $(".dotshow").css({
    	"transition": "all .1s ease-in"
    });
    $(".dotbg").animate({
    	opacity: '1'
    });
    $(".dothide").animate({
    	opacity: '0'
    });
    $(".dotbg2").animate({
    	opacity: '0'
    });

    //Phần text bên Rleft-content
    $(".text-down").animate({
    	opacity: '.2',
    	fontSize: '350%',
    	top: '-5px'
    });
    $(".text-show").animate({
    	opacity: '1',
    	fontSize: '600%',
    	top: '-5px'
    });
    $(".text-up").animate({
    	opacity: '0.2'
    });
    $(".text-down-hide").animate({
    	opacity: '0',
    	top: '-50px'
    });
    $(".Rbtn-explore").css({
    	"background-color": "rgba(72,102,36,.9)",
    	"transition": "all .3s ease-in .2s",
        'transform': 'rotateX(0deg)'
    });

     //Change bacground-img
    $(".lg-slider-2").css({
        'backface-visibility': 'hidden',
        'transform': 'rotateX(90deg)',
        'transition': 'transform 1s ease-in-out .2s',
        'background-color': 'transparent',
        'perspective': '1000px',
        'transform-style': 'preserve-3d'
    });
    $(".lg-slider-1").css({
        'backface-visibility': 'hidden',
        'transform': 'rotateX(360deg)',
        'transition': 'transform 1s ease-in-out .2s',
        'background-color': 'transparent',
        'perspective': '1000px',
        'transform-style': 'preserve-3d'
    });

    //Change bg-color next & prev button
    $(".Rnxt-btn").css({
        'background-color': 'rgba(120,150,120,0.6)'
    });
    $(".Rprev-btn").css({
        'background-color': 'rgba(120,150,120,0.6)'
    });

    //Crete effect of the img(prev)
    // Image 1
    $(".lg-slider-2 .slider-st").css({
        'backface-visibility': 'hidden',
        'transform': 'rotateY(90deg)',
        'transition': 'transform .5s',
        'background-color': 'transparent',
        'perspective': '1000px',
        'transform-style': 'preserve-3d'       
    });
    $(".lg-slider-1 .slider-st").css({
        'backface-visibility': 'hidden',
        'transform': 'rotateY(360deg)',
        'transition': 'transform .5s',
        'background-color': 'transparent',
        'perspective': '1000px',
        'transform-style': 'preserve-3d'       
    });
    // Image 2
    $(".lg-slider-2 .slider-nd").css({
        'backface-visibility': 'hidden',
        'transform': 'rotateY(90deg)',
        'transition': 'transform .7s',
        'background-color': 'transparent',
        'perspective': '1000px',
        'transform-style': 'preserve-3d'   
    });
    $(".lg-slider-1 .slider-nd").css({
        'backface-visibility': 'hidden',
        'transform': 'rotateY(360deg)',
        'transition': 'transform .7s',
        'background-color': 'transparent',
        'perspective': '1000px',
        'transform-style': 'preserve-3d'   
    });
    // Image 3
    $(".lg-slider-2 .slider-rd").css({
        'backface-visibility': 'hidden',
        'transform': 'rotateY(90deg)',
        'transition': 'transform .9s',
        'background-color': 'transparent',
        'perspective': '1000px',
        'transform-style': 'preserve-3d'
    });
    $(".lg-slider-1 .slider-rd").css({
        'backface-visibility': 'hidden',
        'transform': 'rotateY(360deg)',
        'transition': 'transform .9s',
        'background-color': 'transparent',
        'perspective': '1000px',
        'transform-style': 'preserve-3d'
    });
});
});